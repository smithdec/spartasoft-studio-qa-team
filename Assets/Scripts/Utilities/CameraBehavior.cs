using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehavior : MonoBehaviour
{
    private GameObject character;
    private Vector3 pos;
    // Start is called before the first frame update
    void Start()
    {
        character = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        pos = new Vector3(character.transform.position.x, character.transform.position.y, -10);
        this.gameObject.transform.position = pos;
    }
}
