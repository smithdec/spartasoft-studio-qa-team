using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private GameObject inventory;
    // Start is called before the first frame update
    void Start()
    {
        inventory = GameObject.FindWithTag("Inventory");
        inventory.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown("e")) 
        {
            inventory.SetActive(!inventory.activeInHierarchy);
        }
    }
}
