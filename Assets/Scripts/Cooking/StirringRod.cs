using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StirringRod : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private bool clicked = false;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
        RaycastHit hit;
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        bool rc = Physics.Raycast(worldPosition, Vector3.forward, out hit);
        Debug.Log(worldPosition);
        if ((hit.transform.gameObject.name == gameObject.name))
        {
            gameObject.transform.position = Input.mousePosition;
        }
        */
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit, 100)) 
        {
            Debug.Log(hit.transform.name);
            Debug.Log("hit");
        }
    }
}
