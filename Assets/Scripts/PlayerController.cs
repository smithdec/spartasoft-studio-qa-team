using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;
    public int speed = 1000;

    //Doors
    private GameObject House1Door;
    private GameObject House1ExitDoor;

    // Start is called before the first frame update
    void Awake() 
    {
        rb = this.GetComponent<Rigidbody2D>();
        if (GameObject.Find("House1Door") != null) 
        {
            House1Door = GameObject.Find("House1Door");
        }
        if (GameObject.Find("House1ExitDoor") != null) 
        {
            House1ExitDoor = GameObject.Find("House1ExitDoor");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey("w")) 
        {
            rb.AddForce(Vector3.up*speed*Time.deltaTime);
        }
        if(Input.GetKey("a")) 
        {
            rb.AddForce(Vector3.left*speed*Time.deltaTime);
        }
        if(Input.GetKey("d")) 
        {
            rb.AddForce(Vector3.right*speed*Time.deltaTime);
        }
        if(Input.GetKey("s")) 
        {
            rb.AddForce(Vector3.down*speed*Time.deltaTime);
        }
    }

    void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject==House1Door) 
        {
            SceneManager.LoadScene("House1");
            DontDestroyOnLoad(this);
            gameObject.transform.position = new Vector3(House1ExitDoor.transform.position.x,House1ExitDoor.transform.position.y+2,0);
        }
        if(other.gameObject==House1ExitDoor) 
        {
            SceneManager.LoadScene("Overworld");
            DontDestroyOnLoad(this);
            gameObject.transform.position = new Vector3(House1Door.transform.position.x,House1Door.transform.position.y-2,0);
        }
    }
}
